var _ = require('lodash');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var request = require('request');
var router = express.Router();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json()); 

var knex = require('knex')({
    client: 'mysql',
    connection: {
        host: "localhost",
        user: "root",
        password: "",
        database: "employee"
  }
});
var Bookshelf = require('bookshelf')(knex);

// Employee model
var Employee = Bookshelf.Model.extend({
    tableName: 'employee',
    salary: function() {
      return this.hasOne(Salary);
    }
});

// Role model
var Role = Bookshelf.Model.extend({
    tableName: 'role'
});

// Salary model
var Salary = Bookshelf.Model.extend({
    tableName: 'salary'
});

var Employees = Bookshelf.Collection.extend({
    model: Employee
  });
var Roles = Bookshelf.Collection.extend({
    model: Role
  });
var Salaries = Bookshelf.Collection.extend({
    model: Salary
  });

//get all dogs
router.route('/dogs')
.get(function (req, res) {
    request('https://dog.ceo/api/breeds/list/all', function (error, response, body) {
        if(error){
            console.log('error:', error); 
            res.send(error)
        } else{
            console.log('statusCode:', response && response.statusCode);
            res.send(body)
        }
      });
})

//get dog sub breed detail
router.route('/dogs/:breed')
.get(function (req, res) {
  request('https://dog.ceo/api/breed/' + req.params.breed + '/list', function (error, response, body) {
        if(error){
            console.log('error:', error); 
            res.send(error)
        }else{
            
            var obj = JSON.parse(body);
            var subbreed = obj.message;
           
            request('https://dog.ceo/api/breed/' + req.params.breed + '/images', function (error, response, body) {
                if(error){
                    console.log('error:', error); 
                    res.send(error)
                }else{
                    var obj2 = JSON.parse(body);
                    var images = obj2.message;

                    var data = {
                        breed: req.params.breed,
                        sub_breed: subbreed,
                        images: images
                    }
                    res.send(JSON.stringify(data));
                };
              });
        }
      });
})

router.route('/employee')
//get all employee data
.get(function (req, res) {
  Employees.forge()
  .query(function(qb){
    qb.join('salary', 'salary.employee_id','=','employee.id');
    qb.select('full_name','address','dob','role_id','salary');
    })
  .fetch()
  .then(function (collection) {
    res.json({status: '200', data: collection.toJSON()});
  })
  .catch(function (err) {
    res.status(500).json({status: '500', data: {message: err.message}});
  });
})

//insert new employee data
.post(function (req, res) {
  Employee.forge({
    full_name: req.body.full_name,
    address: req.body.address,
    dob: req.body.dob,
    role_id: req.body.role_id
  })
  .save()
  .then(function (Employee) {
    Salary.forge({
        employee_id: Employee.get('id'),
        salary: req.body.salary
      })
      .save()
      .then(function (Salary) {
    res.json({status: '200', data: {id: Salary.get('employee_id')}});
      })
      .catch(function (err) {
        res.status(500).json({status: '500', data: {message: err.message}});
      });
  })
  .catch(function (err) {
    res.status(500).json({status: '500', data: {message: err.message}});
  }); 
});

router.route('/employee/:id')
//get an employee data based on id
.get(function (req, res) {
  Employee.forge({id: req.params.id})
  .query(function(qb){
    qb.join('salary', 'salary.employee_id','=','employee.id');
    qb.select('full_name','address','dob','role_id','salary');
    })
  .fetch()
  .then(function (Employee) {
    if (!Employee) {
      res.status(404).json({status: '404', data: {message: 'Employee not found'}});
    }
    else {
      res.json({status: '200', data: Employee.toJSON()});
    }
  })
  .catch(function (err) {
    res.status(500).json({status: '500', data: {message: err.message}});
  });
})

//edit an employee data
.put(function (req, res) {
    Employee.forge({id: req.params.id})
    .fetch({require: true})
    .then(function (Employee) {
      Employee.save({
        full_name: req.body.full_name || Employee.get('full_name'),
        address: req.body.address || Employee.get('address'),
        dob: req.body.dob || Employee.get('dob'),
        role_id: req.body.role_id || Employee.get('role_id'),
      })
      .then(function () {
        Salary.forge({employee_id: req.params.id})
      .fetch({require: true})
      .then(function (Salary) {
      Salary.save({
        salary: req.body.salary || Salary.get('salary')
      })
      .then(function () {
        res.json({status: '200', data: {message: 'Employee details updated'}});
      })
      .catch(function (err) {
        res.status(500).json({status: '500', data: {message: err.message}});
      });
    })
    .catch(function (err) {
      res.status(500).json({status: '500', data: {message: err.message}});
    });
      })
      .catch(function (err) {
        res.status(500).json({status: '500', data: {message: err.message}});
      });
    })
    .catch(function (err) {
      res.status(404).json({status: '404', data: {message: 'Employee not found'}});
    });
  })

  //delete employee data
  .delete(function (req, res) {
    Employee.forge({id: req.params.id})
    .fetch({require: true})
    .then(function (employee) {
      employee.destroy()
      .then(function () {
        Salary.forge({employee_id: req.params.id})
        .fetch({require: true})
        .then(function (salary) {
          salary.destroy()
          .then(function () {
        res.json({status: '200', data: {message: 'Employee successfully deleted'}});
      })
      .catch(function (err) {
        res.status(500).json({status: '500', data: {message: err.message}});
      });
    })
    .catch(function (err) {
      res.status(500).json({status: '500', data: {message: err.message}});
    });
  })
  .catch(function (err) {
    res.status(500).json({status: '500', data: {message: err.message}});
  });
    })
    .catch(function (err) {
      res.status(404).json({status: '404', data: {message: 'Employee not found'}});
    });
  });

app.use(router);
app.listen(8080);